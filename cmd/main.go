package main

import (
	"fmt"
	"log"

	"github.com/spf13/viper"
	todo "gitlab.com/Mr.Khakimov/gotodolist"
	"gitlab.com/Mr.Khakimov/gotodolist/pkg/handler"
	"gitlab.com/Mr.Khakimov/gotodolist/pkg/repository"
	"gitlab.com/Mr.Khakimov/gotodolist/pkg/service"
)

func main() {
	if err := initConfigs(); err != nil {
		log.Fatalf("error ocured when initializing configs: %s", err.Error())
	}

	repos := repository.NewRepository()
	services := service.NewService(repos)
	handlers := handler.NewHandler(services)

	fmt.Printf("config %s", viper.GetString("port"))

	srv := new(todo.Server)
	if err := srv.Run(viper.GetString("8000"), handlers.InitRoute()); err != nil {
		log.Fatalf("error ocured whem running https server %s", err.Error())
	}
}

func initConfigs() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
